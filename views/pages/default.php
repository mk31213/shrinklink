<form method="post" action="/shrink">
    <input type="text" id="link" value="">
    <input type="submit" class="button" value="Just do it!">
</form>
<div class="new_link"><a></a></div>

<script type="text/javascript">
    $(document).ready(function(){
        $("form").submit(function(e){

            e.preventDefault();
            var data = {'url': $('#link').val()};
            $.ajax({
                type: "POST",
                url: '/shrink',
                data: data,
                success: function(response){
                    $(".new_link").find('a').text(response).attr("href", 'http://' + response)
                }
            });

        });
    })
</script>