<?php

/**
 * Created by PhpStorm.
 * User: Марина
 * Date: 09.06.2016
 * Time: 2:01
 */

namespace view;

class main
{
    private $_manager = null;

    public function __construct(){
        $this->_manager = \SourceManager::create($this);
    }

    private function _construct_page($page_content){
        $o_register = \register::create();
        $layout = 'layouts' . DIRECTORY_SEPARATOR . $o_register->_page_info['layout'] . '.php';
        $layout_path =  $layout;
        $page = $this->get_element($layout_path, array('content'=>$page_content));
        return $this->get_element(
            'common.php',
            array(
                'content' => $page,
                'params' => $this->_manager->getResources()
            )
        );
    }

    public function show_view(Array $info = null){
        $o_register = \register::create();
        $page_path = 'pages' . DIRECTORY_SEPARATOR . $o_register->_page_info['page'] . '.php';
        if(file_exists(VIEW . $page_path)){
            $page_content = $this->get_element(
                $page_path,
                $info
            );
            return $this->_construct_page($page_content);
        } else {
            throw new \c_exception('page does not exist:' . $page_path);
        }
    }

    public function show_ajax($info = null, $output_file){
        $this->use_element('ajax' . DIRECTORY_SEPARATOR . $output_file, $info);
    }

    public function use_element($element, $parameters = array()){
        $element_path = VIEW .$element;

        if(file_exists($element_path)){
            require($element_path);
        } else {
            throw new \c_exception('file does not exist: ' . $element_path);
        }
    }
    public function get_element($element, $parameters = array()){
        ob_start();
        $this->use_element($element, $parameters);
        $return = ob_get_contents();
        ob_end_clean();
        return $return;
    }

    public function addJS($js_files){
        foreach($js_files as $js_file){
            $this->_manager->addJS($js_file);
        }
    }

    public function addCSS($css_files){
        foreach($css_files as $css_file){
            $this->_manager->addCSS($css_file);
        }
    }

}