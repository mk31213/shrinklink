<?php
/**
 * Created by PhpStorm.
 * User: Марина
 * Date: 09.09.2016
 * Time: 22:39
 */
define('APP', __DIR__ . DIRECTORY_SEPARATOR);

require_once APP.'vendor/autoload.php';
require_once APP . 'config/cnfg.php';

use Symfony\Component\HttpFoundation\Request as Request;


$loader = \autoloader::create();


$app = new Silex\Application();

$app->get('/',function() use ($app){
    $o_view = new view\main();
    return $o_view->show_view();
});
$app->get('/url/{key}',function(Request $request, $key) use ($app){
    return $app->redirect((new controller\Links($request))->decodeAction($key));
});
$app->post('/shrink',function(Request $request) use ($app){
    return $_SERVER['HTTP_HOST'] . '/url/' . (new controller\Links($request))->shrinkAction();
});

$app->run();