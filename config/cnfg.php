<?php
/**
 * Created by PhpStorm.
 * User: Марина
 * Date: 10.09.2016
 * Time: 16:55
 */

define('VIEW', APP . 'views' . DIRECTORY_SEPARATOR);
define('WEBROOT',APP . 'webroot' . DIRECTORY_SEPARATOR);
define('MODEL',APP . 'models' . DIRECTORY_SEPARATOR);
define('CONTROLLER',APP . 'controllers' . DIRECTORY_SEPARATOR);

require_once(WEBROOT . 'core' . DIRECTORY_SEPARATOR . 'autoloader.php');