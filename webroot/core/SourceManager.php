<?php
/**
 * Created by PhpStorm.
 * User: mk312_000
 * Date: 03.11.2015
 * Time: 21:37
 */



class SourceManager
{
    private static $_instance = null;

    private $_recource = array(
        'css' => array(),
        'js' => array()
    );

    private $_view = null;

    private function __construct(view\main $view){
        $this->_view = $view;
    }
    private function __clone(){}

    public static function create($view){
        if(is_null(self::$_instance)){
            self::$_instance = new self($view);
        }
        return self::$_instance;
    }

    public function addJS($js_file, $css_files = null){
        if(!is_null($css_files)){
            $this->_view->addCSS($css_files);
        }
        $file = WEBROOT . 'js' . DIRECTORY_SEPARATOR . $js_file;
        if(file_exists($file)){
            $this->_recource['js'][] = '<script type="text/javascript" src="' . '/webroot/js/' . $js_file . '"></script>';
        } else {
            throw new \c_exception('js file does not exist: ' . $file);
        }
    }

    public function addCSS($css_file){
        $file = WEBROOT . 'css' . DIRECTORY_SEPARATOR . $css_file;
        if(file_exists($file)){
            $this->_recource['css'][] = '<link rel="stylesheet" type="text/css" href="' . '/webroot/css/' . $css_file . '">';
        } else {
            throw new \c_exception('css file does not exist: ' . $file);
        }
    }

    public function getResources(){
        $resources = null;
        foreach($this->_recource['css'] as $item){
            $resources .= $item;
        }
        foreach($this->_recource['js'] as $item){
            $resources .= $item;
        }
        return $resources;
    }
}