<?php

/**
 * Created by PhpStorm.
 * User: mk312_000
 * Date: 03.11.2015
 * Time: 21:38
 */
class autoloader
{

    private static $_instance = null;

    private function __clone(){}
    private function __construct(){
        spl_autoload_register(array($this, 'autoload'));
    }

    public static function create(){
        if(is_null(self::$_instance)){
            self::$_instance = new autoloader();
        }
        return self::$_instance;
    }

    public function autoload($name){
        $elements = explode('\\', $name);
        $path = '';
        $namespace = $elements[0];
        switch($namespace){
            case 'controller':
                $path = CONTROLLER;
                break;
            case 'model':
                $path = MODEL;
                break;
            case 'view':
                $path = VIEW;
                break;
            default:
                $path = WEBROOT;
                break;
        }
        return $this->scan($path, $elements[count($elements)-1]);
    }

    private function scan($dir, $name){
        $path = $dir . $name . '.php';
        if(file_exists($path)){
            require_once($path);
            return $path;
        } else {
            $elements = scandir($dir);

            foreach($elements as $item){
                if(!stristr($item, '.')){
                    $result = $this->scan($dir . DIRECTORY_SEPARATOR . $item . DIRECTORY_SEPARATOR, $name);
                    if($result){
                        return $result;
                    }
                }
            }
        }
        return false;
    }
}