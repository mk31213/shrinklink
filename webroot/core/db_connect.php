<?php
/**
 * Created by PhpStorm.
 * User: Марина
 * Date: 10.09.2016
 * Time: 16:59
 */


class db_connect{

    private static $_instance = null;
    private $db = null;
    private $conn = null;

    private function __clone(){}
    private function __construct(){}

    public static function create(){
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function connectDatabase($name){
        if(is_null($this->conn)){
            $this->conn = new \Mongo('localhost');
        }
        if(!isset($this->db[$name]) || is_null($this->db[$name])){
            $this->db[$name] = $this->conn->$name;
        }
        return $this->db[$name];

    }
}