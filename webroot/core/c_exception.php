<?php

/**
 * Created by PhpStorm.
 * User: mk312_000
 * Date: 03.11.2015
 * Time: 21:38
 */
class c_exception extends \Exception
{


    public function __construct($string){
        echo '<br>' . strtoupper('Message: ') . $string;
        $trace = self::getTrace();
        $this->debug($trace);
        die;
    }
    private function debug($input){
            echo '<div class="debug">';
            if(is_array($input)){
                $this->_printArray($input);
            }
            echo '</div>';
    }

    private function _printArray($array){
        foreach($array as $key => $value){
            echo '<div>'.$key . ': ';
            if(is_array($value)){
                $this->_printArray($value);
                echo '</div>';
            } else {
                echo $value.'</div>';
            }
        }
    }

}