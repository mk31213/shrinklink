<?php

/**
 * Created by PhpStorm.
 * User: mk312_000
 * Date: 03.11.2015
 * Time: 21:37
 */
class Register
{
    private static $_instance = null;

    public $_page_info = array();
    private $_protocol = null;
    public $_ajax = false;
    private $links = array();

    private function __construct(){
        $this->_page_info = array(
            'layout' => 'default',
            'page' => 'default',
            'title' => 'Shrink Link',
        );
        $this->_protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
    }
    private function __clone(){}

    public static function create(){
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function _set($name, $value){
        if(empty($this->$name)){
            $this->$name = $value;
        } else {
            throw new \c_exception('can\'t change ' . $name . 'in register');
        }
    }

    public function _get($name){
        return $this->$name;
    }

    public function get_view_settings(){
        return array(
            'layout' => $this->_page_info['layout'],
            'page' => $this->_page_info['page']
        );
    }

    public function getURL($page_id){
        return $this->_protocol . $_SERVER['HTTP_HOST'] . '/' . $this->links[$page_id];
    }

    public function getAjaxPath($controller, $method){
        return sprintf('%s/ajax/%s/%s',
            $this->_protocol . $_SERVER['HTTP_HOST'],
            base64_encode($controller),
            base64_encode($method)
        );
    }

}