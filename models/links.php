<?php
/**
 * Created by PhpStorm.
 * User: Марина
 * Date: 10.09.2016
 * Time: 18:34
 */
namespace model;
use controller;

class Links {

    private static $_instance = null;
    private $db = null;

    private function __clone(){}
    private function __construct(){
        $this->db = \db_connect::create()->connectDatabase('links');
    }

    public static function create(){
        if(is_null(self::$_instance)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getCount(){
        return $this->db->items->find()->count();
    }

    public function newLink($original_url, $short_url){
        $item = array(
            'key' => $short_url,
            'original' => $original_url,
            'count'=>null
        );
        $this->db->items->insert($item);
    }

    public function getOriginal($key){
        $table = $this->db->items;
        $criteria = array(
            'key' => $key
        );
        $record = $table->findOne($criteria);
        $record['count']++;
        $table->save($record);
        return $record['original'];

    }


}