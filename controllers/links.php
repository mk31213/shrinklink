<?php
/**
 * Created by PhpStorm.
 * User: Марина
 * Date: 10.09.2016
 * Time: 17:28
 */
namespace controller;
use model;

class Links {
    protected $request;
    const STEP = 923521;
    const COUNT = 56800235584;

    private $base_string = array(
        'aO60xmkvNsYRKIhMWy3LjcuEQ8HZBXV7eJrCo9TiGf5znFS2APlb4twqdg1DUp',
        'eiBPERUaWLwjbDtSZmY7XMK1JlspcvgA8r3CVF25d0kOGIqxT6N4yHz9ounhQf',
        'U7NnXRAobtwlKry4fBkqP96EsGujSFheMx3IdmHcJ5Z20gCvLO18YiapDzVTWQ',
        'HNT8q4hVdt0pv123C6IJMj5xQRmaOKwuislfyEXoDPczZeBkrAULbYG7nSgF9W',
        'UwfEM149gNKVOHBcnzrsoAxdDtiGeyWQaFP7mCbu85XhLSjZ3p0IR2lTkY6qvJ',
        '4iwN2ut6oRK8HgDxEChTqMf0aQmSP371XvLbdYspJ9UAyzcZeIFWlVO5GBnrkj'
    );

    public function __construct($request) {
        $this->request = $request;
    }
    public function indexAction(){

    }

    public function shrinkAction(){
        $o_model = model\Links::create();
        $original_url = $this->request->get('url');
        $short_url = $this->encodeURL($o_model->getCount());
        $o_model->newLink($original_url,$short_url);
        return $short_url;
    }

    public function decodeAction($key){
        return model\Links::create()->getOriginal($key);
    }

    private function encodeURL($from){
        $next = self::STEP * $from + 1;
        $key = $next > self::COUNT ? ++$next - self::COUNT  : $next;
        $short_url = '';
        foreach($this->base_string as $base){
            $short_url .= $base[$char = $key % 62];
            $key = ($key - $char)/62;

        }
        return $short_url;
    }
}